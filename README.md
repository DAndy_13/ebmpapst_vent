# EbmPapst Fan emulator

EbmPapst Fan emulator. Created so as to have an oportunity to test a communication with EbmPapst fans without HW fans itself.
Emulates both ModBus and EbmPapst's proprietary communication protocol.

![alt tag](../master/images/Vent_test_app.png)