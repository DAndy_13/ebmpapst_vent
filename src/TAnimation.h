//---------------------------------------------------------------------------

#ifndef TAnimationH
#define TAnimationH

#include <vector.h>
#include <vcl.h>


//����� ��������� ��������...
class TAnime
{
private:
    //������ ����������...
    vector<int>     _frame_points;

    //��������
    unsigned int    _interval;
    unsigned int    _tmp_int;


    //��������������� �����
    bool            _reverse;

    //��������� ��������
    int             _tm;       //������ ���������
    int             _NumFrame; //����� �����
public:
        TAnime();
        ~TAnime();
    void Clear();       //������� ������.
    void AddPage(AnsiString FileName); //��������� ����
    void SetInterval(int __interval);
    void InReverse(bool _rev){_reverse = _rev;}; //������������� ����� - �����
    void Draw(TCanvas *c, int X, int Y);
    void Change();
};


//---------------------------------------------------------------------------
#endif
