//---------------------------------------------------------------------------

#ifndef Fan_ebmBUSH
#define Fan_ebmBUSH



#include "types.h"


#define		MAX_SPEED_OLD	1000	// ���� �������� �����������. ������� �� ��� EbmPapst


typedef struct VentExpModule{
	#if _PROJECT_FOR_IAR_
    u16 (*pHandlerReq)(const struct VentExpModule *pVentExpModule, const IdUart_type *pIdUart);
	#else
	u16 (*pHandlerReq)(const struct VentExpModule *pVentExpModule, char *Com);
	#endif
    u16 *adrDev;

    u16 *pBuf;
    u16	count;
    u16 RegAdr;
    u16 *pData;

    u16 *pAvar; 

	u8	*pNeedToWrite;// ������� - �������� ��� ��� 0x06� �������
}VentExpModule_type;






/*
2.1 Command from PC (Master) to Fan (Slave)
	- One preamble (Master - Slave - identification etc.)
	- One command byte (includes command and fan address)
	- One address byte "Fan Group"
	- 0-7 data bytes (depending on type of command) (number defined in preamble)
	- One byte for checksum
2.2 Response from Fan (Slave) to PC (Master)
	- One preamble (Master - Slave - identification etc.)
	- Repetition of the command byte (command and fan address)
	- Repetition of the address byte "Fan Group"
	- 0-7 data bytes (depending on type of command) (number defined in preamble)
	- One byte for checksum
*/



//------------- Preamble --------------------------
/*
The preamble serves as an additional identification of the origin of the telegramme.
If sent by the Master the identification bit is MS = 1.
Response of the addressed Slave sets MS = 0.

All other Slaves recognize this condition and do not have to check subsequent bytes.
This protocol includes the Service bit (Svc) as a new feature:
It is set by the Master (PC) in order to read the (unknown) address of a fan.

Bit 4 is reserved as 1.

Bits identified "x" are synchronization bits at the beginning of a message.
d0, d1, d2 identify the number of data bytes sent.
*/
// d2 d1 d0 1 Svc MS x1 x0
// x: synchronization bits
// MS: Master - Slave � data direction bit
// Svc: service bit
// d0..d2: number of data bytes sent

#define PREAM_STATUS        0x35
//#define PREAM_SET_SPEED     0x55






//------------- Command and Fan Addresses --------------------------
// c2 c1 c0     a4 a3 a2 a1 a0
typedef enum{                   // PC-Fan / Fan-PC
    GetStatus = 0,      //000   // 0 1
    GetActualSpeed,     //001   // 0 1
    SetTargetSpeed,     //010   // 1 0
    SoftwareReset,      //011   // 0 0
    WithoutFunction1,   //100   // - -
    WithoutFunction2,   //101   // - -
    WriteEEPROM,        //110   // 2 0
    ReadEEPROM,         //111   // 1 2
}Fan_CMD_enum;


#define CMD_GET_STATUS(adr)         (GetStatus<<5)|(adr)
#define CMD_GET_SPEED(adr)          (GetActualSpeed<<5)|(adr)
#define CMD_SET_SPEED(adr)          (SetTargetSpeed<<5)|(adr)
#define CMD_SET_EEPROM(adr)         (WriteEEPROM<<5)|(adr)



                                              


u8 CRC_ebmBUS(unsigned char *buf, unsigned char size);

u8 ebmGetStatus(unsigned char *buff, u8 addr, u8 GrAddr);
u8 ebmGetSpeed(unsigned char *buff, u8 addr, u8 GrAddr);
u8 ebmSetSpeed(unsigned char *buff, u8 addr, u8 GrAddr, u8 data);
u8 ebmSetRatio(unsigned char *buff, u8 addr, u8 GrAddr, u8 data);


//---------------------------------------------------------------------------
#endif
