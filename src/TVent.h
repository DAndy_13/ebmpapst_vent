//---------------------------------------------------------------------------

#ifndef TVentH
#define TVentH

#include <vcl.h>
#include "types.h"


typedef struct{
    u16 Vtek;               //(0xD001) ������� ������� (��� ������ � Ebm)

    u16 ActualSpeed;        //(0xD010) ������� ��������
    u16 State;              //(0xD011) ����� ��c������

    u16 Tmodule;            //(0xD015) ����������� ������
    u16 Tmotor;             //(0xD016) ����������� ������
    u16 Tint;				//(0xD017) ����������� ����������� (����������)
    u16 CurDir;				//(0xD018) ������� ����������� ��������


    u16 Dir;                //(0xD102) ����������� �������� (��� ������ � Ebm)
	    						// 0 - ������ �������;
		    					// 1 - �� �������;

    u16 VtekPers;           // ��� ����������� � ���������
    u16 SpeedObMin;         // ��� ����������� � ��/���

}Vent_type;
#define OFFSET_Vent		(*(Vent_type*)0)



class TVent
{
    private:
        static u16 Counter;
    protected:

    public:
    	u08 NetAdr;				//������� ����� �����������.
    	u16 MaxSpeed;  		    // ���� �������
    	u16 MaxValue;  		    // ���� Vtek

        Vent_type Data;

        TVent(u16 _NetAdr);
        ~TVent(){Counter--;};

        void    SetNetAdr(u16 _NetAdr){this->NetAdr = _NetAdr;};
        u16     GetNetAdr(){return this->NetAdr;};

        virtual u08 ProcessMsg(u08 *Bff, u08 len) = 0;
        virtual void TimeProc() = 0;
        virtual AnsiString Print() = 0;
};


u16 TVent::Counter = 0;



//---------------------------------------------------------------------------
#endif
