//---------------------------------------------------------------------------

#ifndef modbusH
#define modbusH

#include "types.h"

#define Func_0x03   0x03
#define Func_0x04   0x04
#define Func_0x05   0x05
#define Func_0x06   0x06
#define Func_0x10   0x10
#define Func_0x2B   0x2B
#define Func_0x67   0x67
#define Func_0x68   0x68
#define Func_0x69   0x69




typedef union
{
    u08     mByte[4];
    u16     mShort[2];
    long    mlong;
} uLong;


typedef union
{
    u08     mByte[4];
    u16     mShort[2];
    float   mfloat;
} uFloat;



    int ModbusFunc_03(  unsigned char *buff, unsigned short addr, unsigned short count, char Slave_Addr);
    int ModbusFunc_04(  unsigned char *buff, unsigned short addr, unsigned short count, char Slave_Addr);
    int ModbusFunc_06(  unsigned char *buff, unsigned short addr, unsigned short data, char Slave_Addr);
    int ModbusFunc_05(  unsigned char *buff, unsigned short addr, unsigned short data, char Slave_Addr);
//    int ModbusFunc_10(unsigned char *buff, unsigned short addr, unsigned short* data,
//                        unsigned short count, char Slave_Addr);
    int ModbusFunc_10(  unsigned char   *buff,
                        unsigned short  addr,
                        unsigned short  *data,
                        unsigned short  count,
                        unsigned char   Slave_Addr);

    int ModbusFunc_67(  unsigned char   *buff,
                        unsigned long   addr,
                        unsigned short  count,
                        unsigned char   Slave_Addr);

    int ModbusFunc_68(  unsigned char   *buff,
                        unsigned long   AddrInFile,
                        unsigned short  count,
                        unsigned char   Slave_Addr,
                        unsigned char   NumFile);

    int ModbusFunc_69(  unsigned char   *buff,
                        unsigned long   AddrInFile,
                        unsigned short  *pData,
                        unsigned short  count,
                        unsigned char   Slave_Addr,
                        unsigned char   NumFile);

    int ModbusFunc_2B(unsigned char *buff, unsigned char NetAdr, unsigned char DeviceID, unsigned char ObjID);
    
    int GetLenAnswer(u08 func, u08 count);                        

//---------------------------------------------------------------------------
#endif
