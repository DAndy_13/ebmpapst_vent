//---------------------------------------------------------------------------


#pragma hdrstop

#include "TVent_EbmBus.h"
#include "Fan_ebmBUS.h"



//u8 TVent_EbmBus::CRC_ebmBUS(unsigned char *buf, unsigned char size)
u8 CRC_ebmBUS(unsigned char *buf, unsigned char size)
{
    u8 CRC = 0xFF;

    for (; size > 0; size --){
        CRC ^= *buf++;
    }

    return CRC&0xFF;
}





TVent_EbmBus::TVent_EbmBus(u16 _NetAdr): TVent(_NetAdr)
{
    this->MaxSpeed = MAX_SPEED_OLD;
    this->MaxValue = 250;
}
//-------------------------------------------------


void TVent_EbmBus::TimeProc()
{
    float   Pers    = this->Data.VtekPers * 1.0;    
    u08     Vtek    = this->Data.Vtek;

   	this->Data.VtekPers         = (Vtek*100)/this->MaxValue;

    u16 ActualSpeed = this->Data.ActualSpeed;

    if(this->Data.CurDir != this->Data.Dir){            // ����� �����������
        if(ActualSpeed > 0){
            Vtek = 0;
        }
        else{
            this->Data.CurDir = this->Data.Dir;
        }
    }

    if(ActualSpeed < Vtek){                             // ������
        ActualSpeed = ActualSpeed + (Vtek - ActualSpeed)*0.1 +1;
    }
    else if(ActualSpeed > Vtek){                        // ����������
        ActualSpeed = ActualSpeed - (ActualSpeed - Vtek)*0.05;
    }

   	this->Data.ActualSpeed  = ActualSpeed;
  	this->Data.SpeedObMin   = (ActualSpeed*this->MaxSpeed)/this->MaxValue;
}
//-------------------------------------------------



u08 TVent_EbmBus::ProcessMsg(u08 *Bff, u08 len){
    int sz;
    u08 CrcVal;
    u08 Adr = Bff[1]&0x1F;

    if (Adr != this->NetAdr){
        Adr++; Adr--;
        return 0;//�������� ������ � ��
    }
    if(CRC_ebmBUS(Bff,len-1) != Bff[len-1]){
        CrcVal++; CrcVal--;
        return 0;//�������� ������ � ��
    }

    u16 CMD = (Bff[0]<<8)|(Bff[1]&(~0x1F));
    switch (CMD)     // ����� �������
    {
        case (u16)(0x35<<8)|CMD_GET_STATUS(0):  sz = ebmGetStatus(Bff,len); break;
        case (u16)(0x15<<8)|CMD_GET_SPEED(0):   sz = ebmGetSpeed(Bff,len);  break;//������� 03 � 04
        case (u16)(0x35<<8)|CMD_SET_SPEED(0):   sz = ebmSetSpeed(Bff,len);  break;//������� 03 � 04
        default:
//            Bff[CMD_OFFSET] |= 0x80;
//            Bff[SIZE_OFFSET] = 1;
            sz = 0;
    }
  
    if ((!sz) || (sz > 255)) return 0;

    return sz;//+1;
}
//-------------------------------------------------



u08 TVent_EbmBus::ebmGetStatus(u08 *Bff, u08 len)
{
    u08 sz = 4;
    u16 *pBuffIn = (u16 *)&Bff[4];

    *pBuffIn = this->Data.State;

    u08 CrcVal = CRC_ebmBUS(Bff, 5);
    Bff[5] = CrcVal;


    return 6;
}
//-------------------------------------------------

u08 TVent_EbmBus::ebmGetSpeed(u08 *Bff, u08 len)
{
    u08 sz = 4;
    u16 *pBuffIn = (u16 *)Bff[3];

    //actual speed [1/min] = (data byte / 250) * nMax [1/min]

    u16 DataByte = this->Data.ActualSpeed;
    //*pBuffIn = (u08)DataByte;
    Bff[3] = (u08)DataByte;

    u08 CrcVal = CRC_ebmBUS(Bff, sz);
    Bff += sz;
    *Bff = CrcVal;

    return sz+1;
}
//-------------------------------------------------



u08 TVent_EbmBus::ebmSetSpeed(u08 *Bff, u08 len)
{
    u08 sz = 3;

    u16 DataByte = (u16)Bff[3];

    this->Data.Vtek = DataByte;//(DataByte*this->MaxSpeed)/this->MaxValue;

    u08 CrcVal = CRC_ebmBUS(Bff, sz);
    Bff += sz;
    *Bff = CrcVal;

    return sz+1;
}
//-------------------------------------------------




//---------------------------------------------------------------------------

#pragma package(smart_init)
