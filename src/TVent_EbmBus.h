//---------------------------------------------------------------------------

#ifndef TVent_EbmBusH
#define TVent_EbmBusH

#include "TVent.h"



class TVent_EbmBus: public TVent
{
    private:    
        u08 ebmGetStatus(u08 *Bff, u08 len);
        u08 ebmGetSpeed(u08 *Bff, u08 len);
        u08 ebmSetSpeed(u08 *Bff, u08 len);

    public:
        TVent_EbmBus(u16 _NetAdr);
        ~TVent_EbmBus(){};

        //u8 CRC_ebmBUS(unsigned char *buf, unsigned char size);

        u08 ProcessMsg(u08 *Bff, u08 len);
        void TimeProc();
        AnsiString Print(){return "EbmBus NetAdr = " + AnsiString(this->NetAdr);}
};



u8 CRC_ebmBUS(unsigned char *buf, unsigned char size);

//---------------------------------------------------------------------------
#endif
