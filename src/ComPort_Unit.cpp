//---------------------------------------------------------------------------


#pragma hdrstop

#include "ComPort_Unit.h"
//#include "mDefinitions.h"

#include "main.h"

//    unsigned char bufrd[BUFSIZE], bufwr[BUFSIZE]; //������� � ���������� ������

//---------------------------------------------------------------------------

    HANDLE hCOMport;		//���������� �����

//��������� OVERLAPPED ���������� ��� ����������� ��������, ��� ���� ��� �������� ������ � ������ ����� �������� ������ ���������
//��� ��������� ���������� �������� ���������, ����� ��������� �� ����� �������� ���������
    OVERLAPPED overlapped;		//����� ������������ ��� �������� ������ (��. ����� ReadThread)
    OVERLAPPED overlappedwr;       	//����� ������������ ��� �������� ������ (��. ����� WriteThread)

//---------------------------------------------------------------------------



    unsigned long counter;	//������� �������� ������, ���������� ��� ������ �������� �����







/********* ������� �������� � ������������� ����� *************
*  portname = ��� ����� (��������, "COM1", "COM2" � �.�.)
*  BaudRate = �������� ��������
***************************************************************/
AnsiString COMOpen(HANDLE *hport, AnsiString portname, int BaudRate)
{
    DCB dcb;                //��������� ��� ����� ������������� ����� DCB
    COMMTIMEOUTS timeouts;  //��������� ��� ��������� ���������

    //���� ������� "COMn", ������ "\\\\.\\COMn" ����� ������ ��� ����� ����� ��������� (1-9).
    portname = "\\\\.\\" + portname;
    
    //������� ����, ��� ����������� �������� ����������� ����� ������� ���� FILE_FLAG_OVERLAPPED
    *hport = CreateFile(portname.c_str(),
                        GENERIC_READ | GENERIC_WRITE,
                        0,
                        NULL,
                        OPEN_EXISTING,
                        0,//FILE_FLAG_OVERLAPPED,
                        NULL);
 //�����:
 // - portname.c_str() - ��� ����� � �������� ����� �����, c_str() ����������� ������ ���� String � ������ � ���� ������� ���� char, ����� ������� �� ������
 // - GENERIC_READ | GENERIC_WRITE - ������ � ����� �� ������/�������
 // - 0 - ���� �� ����� ���� ������������� (shared)
 // - NULL - ���������� ����� �� �����������, ������������ ���������� ������������ �� ���������
 // - OPEN_EXISTING - ���� ������ ����������� ��� ��� ������������ ����
 // - FILE_FLAG_OVERLAPPED - ���� ���� ��������� �� ������������� ����������� ��������
 // - NULL - ��������� �� ���� ������� �� ������������ ��� ������ � �������

    if(*hport == INVALID_HANDLE_VALUE)            //���� ������ �������� �����
    {
//        return (GetMessageText(pWSpace->Language, enErrOpenPort) + portname);
        return "������ �������� ����� " + portname;
    }

 //������������� �����

    dcb.DCBlength = sizeof(DCB); 	//� ������ ���� ��������� DCB ���������� ������� � �����, ��� ����� �������������� ��������� ��������� ����� ��� �������� ������������ ���������

    //������� ��������� DCB �� �����
    if(!GetCommState(*hport, &dcb))	//���� �� ������� - ������� ���� � ������� ��������� �� ������ � ������ ���������
    {
        COMClose(hport);
        //return (GetMessageText(pWSpace->Language, enReadPortConfig) + portname);
        return "������ ������ ���������� ����� "+ portname;
    }

 //������������� ��������� DCB
//    dcb.flags           = 0x0001;

    dcb.DCBlength       = sizeof(DCB);          //����� �������� ��������
    dcb.BaudRate        = BaudRate;             //����� �������� ��������
    dcb.ByteSize        = 8;                    //����� 8 ��� � �����
    if(mVent.VentType == VentModBus){
        dcb.Parity          = EVENPARITY;           //��������� �������� ��������
    }
    else{
        dcb.Parity          = NOPARITY;             //��������� �������� ��������
    }

    dcb.StopBits        = ONESTOPBIT;           //����� ���� ����-���

    dcb.fNull           = FALSE;                //��������� ���� ������� ������
    dcb.fBinary         = TRUE;
    dcb.fParity         = NOPARITY;
    dcb.fOutxCtsFlow    = FALSE;                //��������� ����� �������� �� �������� CTS
    dcb.fOutxDsrFlow    = FALSE;                //��������� ����� �������� �� �������� DSR
    dcb.fDtrControl     = DTR_CONTROL_DISABLE;  //��������� ������������� ����� DTR
    dcb.fDsrSensitivity = FALSE;                //��������� ��������������� �������� � ��������� ����� DSR
    dcb.fOutX           = FALSE;
    dcb.fInX            = FALSE;
    dcb.fErrorChar      = FALSE;
    dcb.fRtsControl     = RTS_CONTROL_DISABLE;  //��������� ������������� ����� RTS
    dcb.fAbortOnError   = TRUE;                 //��������� ��������� ���� �������� ������/������ ��� ������

    dcb.XonChar         = 0;
    dcb.XoffChar        = 0;
    dcb.ErrorChar       = 0;
    dcb.EofChar         = 0;
    dcb.EvtChar         = 0;


 //��������� ��������� DCB � ����
    if(!SetCommState(*hport, &dcb))	//���� �� ������� - ������� ���� � ������� ��������� �� ������ � ������ ���������
    {
        COMClose(hport);
        //return (GetMessageText(pWSpace->Language, enErrWritePortConfig) + portname);
        return "������ ������ ���������� ����� "+ portname;
    }

 //���������� ��������
    timeouts.ReadIntervalTimeout =40+(100000/BaudRate);         //������� ����� ����� ���������
    timeouts.ReadTotalTimeoutMultiplier =2*(10000/BaudRate);    //����� ������� �������� ������
    timeouts.ReadTotalTimeoutConstant = 300;//1500;//200;//500;//1000;                    //��������� ��� ������ �������� �������� ������
    timeouts.WriteTotalTimeoutMultiplier =0;                    //����� ������� �������� ������
    timeouts.WriteTotalTimeoutConstant =0;                      //��������� ��� ������ �������� �������� ������


 //�������� ��������� ��������� � ����
    if(!SetCommTimeouts(*hport, &timeouts))	//���� �� ������� - ������� ���� � ������� ��������� �� ������ � ������ ���������
    {
        COMClose(hport);
        //return (GetMessageText(pWSpace->Language, enErrWrTimeOuts) + portname);
        return "������ ������ ��������� ����� "+ portname;
    }

 //���������� ������� �������� ����� � ��������
    SetupComm(*hport,2000,2000);

    PurgeComm(*hport, PURGE_RXCLEAR);	//�������� ����������� ����� �����

// reader = new ReadThread(false);	//������� � ��������� ����� ������ ������
// reader->FreeOnTerminate = true;        //���������� ��� �������� ������, ����� �� ������������� ����������� ����� ����������

// writer = new WriteThread(true);               //������� ����� ������ ������ � ����
// writer->FreeOnTerminate = true;                //���������� ��� ��������, ����� ����� ������������� ����������� ����� ����������
    return "";//"������ ����:  " + portname;
}
//---------------------------------------------------------------------------




//������� �������� �����
void COMClose(HANDLE *hport)
{
/*
 //���� ����� ������ ����������, ������ ��� ������� �� ���������� � ��������� ���, ����� �� �������� ����������;
 if(writer) //�������� if(writer) �����������, ����� ��������� ������;
  {
   writer->Terminate();
   writer->Resume();
  }

//    if(reader)reader->Terminate();         //���� ����� ������ ��������, ��������� ���; �������� if(reader) �����������, ����� ��������� ������
*/
    CloseHandle(*hport);     //������� ����
    *hport=0;				//�������� ���������� ��� ����������� �����
}

//---------------------------------------------------------------------------



// ������� ���������� ��� ��������� ����� � pComName
void EnumerateComPorts(TComboBox* pComName)
{
    TRegistry *reg = new TRegistry;
    TStringList *list = new TStringList;
    pComName->Items->Clear();

    reg->RootKey = HKEY_LOCAL_MACHINE;
    reg->OpenKey("HARDWARE\\DEVICEMAP\\SERIALCOMM",false);
    reg->GetValueNames(list);

    for(int i=0;i<list->Count;i++)
        pComName->Items->Add(reg->ReadString(list->Strings[i]));
        
    pComName->ItemIndex = 0;
    delete list;
    delete reg;
}
//---------------------------------------------------------------------------

#pragma package(smart_init)
