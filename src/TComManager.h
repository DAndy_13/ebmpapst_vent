//---------------------------------------------------------------------------

#ifndef TComManagerH
#define TComManagerH

//---------------------------------------------------------------------------
#include <Classes.hpp>
//---------------------------------------------------------------------------

#include "types.h"
#include "ComPort_Unit.h"
//#include "DataPool.h"
#include "crc_16.h"


typedef struct
{
    u08     buff[256];
    u16     len;
}Mess_type;



class TComManager : public TThread
{            
private:
protected:
    void __fastcall Execute();

    void GetErrString(String *StateStr, DWORD dwErr);
public:

    Mess_type TxMess, RxMess;

    __fastcall TComManager(bool CreateSuspended);
};
//---------------------------------------------------------------------------
#endif
 