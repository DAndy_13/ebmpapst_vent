//---------------------------------------------------------------------------

#ifndef crc_16H
#define crc_16H

extern unsigned short CRC;              

unsigned int CRC16(unsigned char *buf, unsigned char size);
unsigned int CRC16L(unsigned char *buf, unsigned long size);

unsigned char CRC8(unsigned char *pcBlock, unsigned char len);

void memcpy_new(void *s1, const void *s2, int n);

//---------------------------------------------------------------------------
#endif
