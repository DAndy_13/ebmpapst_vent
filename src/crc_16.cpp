//---------------------------------------------------------------------------
#pragma hdrstop

#include "crc_16.h"

unsigned short CRC;


unsigned int CRC16(unsigned char *buf, unsigned char size)
{
  unsigned int CRC = 0xffff;
  unsigned int temp;
  for (; size > 0; size --)
  {
    CRC ^= *buf ++;
    for (unsigned char j = 0; j < 8; j++)
    {
		  if (CRC & 0x0001) temp = 1;
		  else temp = 0;
		  CRC = CRC >> 1;
		  if (temp) CRC ^= 0xA001;
    }
  }
  temp = CRC;
  CRC = CRC << 8;
  temp = temp >> 8;
  temp &= 0x00FF;
  CRC |= temp;
  return CRC&0xFFFF;
}
//---------------------------------------------------------------------------


unsigned int CRC16L(unsigned char *buf, unsigned long size)
{
  unsigned int CRC = 0xffff;
  unsigned int temp;
  for (; size > 0; size --)
  {
    CRC ^= *buf ++;
    for (unsigned char j = 0; j < 8; j++)
    {
		  if (CRC & 0x0001) temp = 1;
		  else temp = 0;
		  CRC = CRC >> 1;
		  if (temp) CRC ^= 0xA001;
    }
  }
  temp = CRC;
  CRC = CRC << 8;
  temp = temp >> 8;
  temp &= 0x00FF;
  CRC |= temp;
  return CRC&0xFFFF;
}
//---------------------------------------------------------------------------



/*
  Name  : CRC-8
  Poly  : 0x31    x^8 + x^5 + x^4 + 1
  Init  : 0xFF
  Revert: false
  XorOut: 0x00
  Check : 0xF7 ("123456789")
  MaxLen: 15 ����(127 ���) - �����������
    ���������, �������, ������� � ���� �������� ������
*/
                     
unsigned char CRC8(unsigned char *pcBlock, unsigned char len)
{
    unsigned char crc = 0xFF;
    unsigned int i;

	crc ^= *pcBlock++;
    for (i = 0; i < 8; i++)
		crc = crc & 0x80 ? (crc << 1) ^ 0x31 : crc << 1;
	len-=2; pcBlock++;

    while (len--)
    {
        crc ^= *pcBlock++;

        for (i = 0; i < 8; i++)
            crc = crc & 0x80 ? (crc << 1) ^ 0x31 : crc << 1;
    }

    return crc;
}
//---------------------------------------------------------------------------




void memcpy_new(void *s1, const void *s2, int n)
{
  unsigned char *v;

  v = (unsigned char*) s1;
  while (n--)
  {
    unsigned short tmp = *(unsigned short*)s2;
    *v++ = (tmp >> 8);
    *v++ = tmp;
    s2 = (unsigned short*)s2 + 1;
  }
}
//---------------------------------------------------------------------------

#pragma package(smart_init)
