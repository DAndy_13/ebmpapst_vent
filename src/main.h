//---------------------------------------------------------------------------

#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "sButton.hpp"
#include "sSkinManager.hpp"
#include <Grids.hpp>
#include "sComboBox.hpp"
#include "sPanel.hpp"
#include <ExtCtrls.hpp>
#include "sTrackBar.hpp"
#include <ComCtrls.hpp>
#include "sGroupBox.hpp"
#include "sCheckBox.hpp"
#include <Graphics.hpp>

#include "types.h"

typedef enum{
    VentModBus = 0,
    VentEbmBus,
}enVentType;


typedef struct{
    u16 VentCount;
    enVentType VentType;
}mVent_type;

extern mVent_type mVent;


#include "TVent.h"



typedef enum // ���������
{
    WM_REFRESH = WM_USER + 1,

    WM_POOL,
    WM_POOL_MANAGER,

    WM_MAIL_MSG,

//    WM_MASTER,
}WM_Enum;



//---------------------------------------------------------------------------
class TF_Main : public TForm
{
__published:	// IDE-managed Components
    TsSkinManager *sSkinManager1;
    TStringGrid *StringGrid1;
    TsPanel *sPanel1;
    TButton *Button1;
    TsButton *sButton1;
    TsButton *sButton2;
    TsTrackBar *sTrackBar1;
    TsGroupBox *sGroupBox1;
    TsGroupBox *sGroupBox2;
    TsComboBox *CountCombo;
    TsComboBox *TypeCombo;
    TButton *Button2;
    TsGroupBox *sGroupBox3;
    TsComboBox *PortCombo;
    TsCheckBox *sCheckBox1;
    TTimer *MainTimer;
    TButton *Button3;
    TsTrackBar *sTrackBar2;
    TTimer *AnimationTimer;
    void __fastcall sButton1Click(TObject *Sender);
    void __fastcall Button1Click(TObject *Sender);
    void __fastcall TypeComboChange(TObject *Sender);
    void __fastcall sButton2Click(TObject *Sender);
    void __fastcall StringGrid1DrawCell(TObject *Sender, int ACol,
          int ARow, TRect &Rect, TGridDrawState State);
    void __fastcall StringGrid1DblClick(TObject *Sender);
    void __fastcall sTrackBar1Exit(TObject *Sender);
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall CountComboChange(TObject *Sender);
    void __fastcall Button2Click(TObject *Sender);
    void __fastcall sCheckBox1Click(TObject *Sender);
    void __fastcall Button3Click(TObject *Sender);
    void __fastcall MainTimerTimer(TObject *Sender);
    void __fastcall AnimationTimerTimer(TObject *Sender);
private:	// User declarations
    bool bNullCell;
    void GridIni(TStringGrid *Grid, u08 VentCount, enVentType VentType);
    void GridFill(TStringGrid *Grid, TVent *pVent);
    void GridShowEdit(TStringGrid *Grid, int ACol, int ARow, u16 Value);//TVent *pVent);
    void SetBkColor(TStringGrid *Grid, TColor Color);


public:		// User declarations
    __fastcall TF_Main(TComponent* Owner);

    void __fastcall WmUser_catch (TMessage &msg);


BEGIN_MESSAGE_MAP
    MESSAGE_HANDLER ( WM_REFRESH,        TMessage , WmUser_catch )

    //� �.�. ��� ������ ��������� ��� ���� �������
END_MESSAGE_MAP(TForm)//TComponent)

};
//---------------------------------------------------------------------------
extern PACKAGE TF_Main *F_Main;



//---------------------------------------------------------------------------
#endif
