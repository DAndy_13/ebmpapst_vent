//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "TComManager.h"
#include "main.h"
#include "TVent.h"
#include "TVent_EbmBus.h"

#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall TComManager::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------


    static AnsiString StateStr = "";



__fastcall TComManager::TComManager(bool CreateSuspended)
    : TThread(CreateSuspended)
{
    memset(TxMess.buff, 0, sizeof(TxMess.buff));
    memset(RxMess.buff, 0, sizeof(RxMess.buff));
}
//---------------------------------------------------------------------------





void TComManager::GetErrString(String *StateStr, DWORD dwErr)
{
    switch(dwErr)
    {
        case ERROR_ACCESS_DENIED:   // 5L
                *StateStr = "Access is denied.";//GetMessageText(pWSpace->Language, enAccessDenied);// �������
                break;

        case ERROR_INVALID_HANDLE:  // 6L
                *StateStr = "The handle is invalid.";//GetMessageText(pWSpace->Language, enInvalidHandle);// �������
                break;

        case ERROR_GEN_FAILURE:     // 31L
                *StateStr = "Device attached to the system is not functioning";//GetMessageText(pWSpace->Language, enErrDeviceAttach);// �������
                break;

        case ERROR_OPERATION_ABORTED:     // 995L
                *StateStr = "I/O operation has been aborted";//GetMessageText(pWSpace->Language, enErrIOAborted);// �������
                break;

        default:
                break;
    }
}




extern TVent *VentArr[];

void __fastcall TComManager::Execute()
{
    TVent *pVent;
    //---- Place thread code here ----

    bool ComRes;
    DWORD dwError;
    u32 count = 0, sz = 0;
    u08 indx = 0;

    COMSTAT ComStat;
    unsigned long ComErr;

    
    while(!Terminated){
        //Sleep(1);

        //cnt = 255;
        //PurgeComm(hCOMport, PURGE_RXCLEAR);	//�������� ����������� ����� �����
        count = 0;
        ClearCommError(hCOMport, &ComErr, &ComStat);
        u16 cnt = 255;//ComStat.cbInQue;
        ComRes = ReadFile(hCOMport, &RxMess.buff[RxMess.len], cnt, &count, NULL);     //��������� ����� �� ����� � ����� ���������
        dwError = GetLastError();
        if((!ComRes)&&(dwError != NO_ERROR))
        {
            GetErrString(&StateStr, dwError);// ������ !!!
        }
        if(count >= 4) // �������� ���� ���-��
        {
            RxMess.len += count;

            if(mVent.VentType == VentModBus)
            {
                u16 CRC = (u16)((RxMess.buff[count - 2] << 8) + RxMess.buff[count - 1]);
                u16 CRCmess = CRC16(RxMess.buff, count-2);
                indx = RxMess.buff[0];
                if((CRC == (CRCmess&0xFFFF))&&(indx >0))
                {             
                    indx -= 1;
                    if(indx < mVent.VentCount){
                        pVent = VentArr[indx];
                    }
                    else{
                        pVent = 0;
                    }

                    if(pVent){
                        sz = pVent->ProcessMsg(RxMess.buff, RxMess.len);
                        if(sz){
                            memcpy(TxMess.buff, RxMess.buff, sz);
                            TxMess.len = sz;
                        }
                    }
                    RxMess.len = 0;
                    pVent = 0;
                }
                else{
                    CRC++; CRC--;
                }
                PurgeComm(hCOMport, PURGE_RXCLEAR);	//�������� ����������� ����� �����
                memset(RxMess.buff, 0, sizeof(RxMess.buff));
                RxMess.len = 0;
                pVent = 0;
            }
            else
            {
                u08 CRC = RxMess.buff[count - 1];
                u08 CRCmess = CRC_ebmBUS(RxMess.buff, count-1);
                indx = (RxMess.buff[1]&0x1F);
                if((CRC == (CRCmess&0xFF))&&(indx >0))
                {
                    indx -= 1;                    
                    if(indx < mVent.VentCount){
                        pVent = VentArr[indx];
                    }
                    else{
                        pVent = 0;
                    }

                    if(pVent){
                        sz = pVent->ProcessMsg(RxMess.buff, RxMess.len);
                        if(sz){
                            memcpy(TxMess.buff, RxMess.buff, sz);
                            TxMess.len = sz;
                        }
                        else{
                            sz++; sz--;
                        }
                    }
                    //memset(RxMess.buff, 0, sizeof(RxMess.buff));
                    RxMess.len = 0;
                    pVent = 0;
                }
                else{
                    CRC++; CRC--;
                }
                PurgeComm(hCOMport, PURGE_RXCLEAR);	//�������� ����������� ����� �����
                memset(RxMess.buff, 0, sizeof(RxMess.buff));
                RxMess.len = 0;
                pVent = 0;
            }
        }
        else{
            count = 0;
            Sleep(1);
        }


        if(TxMess.len){
            PurgeComm(hCOMport, PURGE_RXCLEAR);	//�������� ����������� ����� �����
            PurgeComm(hCOMport, PURGE_TXCLEAR);	//�������� ���������� ����� �����
            ComRes = WriteFile(hCOMport, TxMess.buff, TxMess.len, &count, NULL);
            //memset(TxMess.buff, 0, sizeof(TxMess.buff));
            TxMess.len = 0;
        }

    }
}
//---------------------------------------------------------------------------
 