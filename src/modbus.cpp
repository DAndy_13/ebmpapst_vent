#pragma hdrstop
//---------------------------------------------------------------------------
#include "modbus.h"
#include "crc_16.h"





int ModbusFunc_03(unsigned char *buff, unsigned short addr, unsigned short count, char Slave_Addr)
{
    int Tx_Len = 0;
    buff[Tx_Len++] = Slave_Addr;     // ����� ������������
    buff[Tx_Len++] = Func_0x03;      // �������

    buff[Tx_Len++] = addr >> 8;      // ����� �������� ��.
    buff[Tx_Len++] = addr;           // ����� �������� ��.

    buff[Tx_Len++] = count>>8;       // ������ ��.
    buff[Tx_Len++] = count;          // ������ ��.

    CRC = CRC16(&buff[0], Tx_Len);

    buff[Tx_Len++] = CRC>>8;    	    // CRC ��.
    buff[Tx_Len++] = CRC;            // CRC ��.

    return Tx_Len;
}
//---------------------------------------------------------------------------


int ModbusFunc_04(unsigned char *buff, unsigned short addr, unsigned short count, char Slave_Addr)
{
    int Tx_Len = 0;
    buff[Tx_Len++] = Slave_Addr;     // ����� ������������
    buff[Tx_Len++] = Func_0x04;      // �������

    buff[Tx_Len++] = addr >> 8;      // ����� �������� ��.
    buff[Tx_Len++] = addr;           // ����� �������� ��.

    buff[Tx_Len++] = count>>8;       // ���-�� ��.
    buff[Tx_Len++] = count;          // ���-�� ��.

    CRC = CRC16(&buff[0], Tx_Len);

    buff[Tx_Len++] = CRC>>8;    	 // CRC ��.
    buff[Tx_Len++] = CRC;            // CRC ��.

    return Tx_Len;
}
//---------------------------------------------------------------------------


int ModbusFunc_06(unsigned char *buff, unsigned short addr, unsigned short data, char Slave_Addr)
{
    int Tx_Len = 0;
    buff[Tx_Len++] = Slave_Addr;   // ����� ������������
    buff[Tx_Len++] = Func_0x06;    // �������

    buff[Tx_Len++] = addr >> 8;    // ����� �������� ��.
    buff[Tx_Len++] = addr;         // ����� �������� ��.

    buff[Tx_Len++] = data>>8;      // ������ ��.
    buff[Tx_Len++] = data;         // ������ ��.

    CRC = CRC16(&buff[0], Tx_Len);

    buff[Tx_Len++] = CRC>>8;       // CRC ��.
    buff[Tx_Len++] = CRC;          // CRC ��.

    return Tx_Len;
}

//---------------------------------------------------------------------------



int ModbusFunc_05(unsigned char *buff, unsigned short addr, unsigned short data, char Slave_Addr)
{
    int Tx_Len = 0;
    buff[Tx_Len++] = Slave_Addr;   // ����� ������������
    buff[Tx_Len++] = Func_0x05;    // �������

    buff[Tx_Len++] = addr >> 8;    // ����� �������� ��.
    buff[Tx_Len++] = addr;         // ����� �������� ��.

    buff[Tx_Len++] = data>>8;      // ������ ��.
    buff[Tx_Len++] = data;         // ������ ��.

    CRC = CRC16(&buff[0], Tx_Len);

    buff[Tx_Len++] = CRC>>8;       // CRC ��.
    buff[Tx_Len++] = CRC;          // CRC ��.

    return Tx_Len;
}
//---------------------------------------------------------------------------



int ModbusFunc_10(unsigned char     *buff,
                  unsigned short    addr,
                  unsigned short    *data,
                  unsigned short    count,
                  unsigned char     Slave_Addr)
{
    int Tx_Len  = 0;

    buff[Tx_Len++] = Slave_Addr;    // ����� ������������
    buff[Tx_Len++] = Func_0x10;//ModBus_Func;   // �������

    buff[Tx_Len++] = addr >> 8;    // ����� �������� ��.
    buff[Tx_Len++] = addr;         // ����� �������� ��.

    buff[Tx_Len++] = count>>8;       // ���-�� ��.
    buff[Tx_Len++] = count;          // ���-�� ��.

    buff[Tx_Len++] = count*2;        // ������� ����


        {
        buff[Tx_Len++] = (*data)>>8;
        buff[Tx_Len++] = (*data);
        buff[Tx_Len++] = (*++data)>>8;
        buff[Tx_Len++] = (*data);
        }

    CRC = CRC16(&buff[0], Tx_Len);

    buff[Tx_Len++] = CRC>>8;    // CRC ��.
    buff[Tx_Len++] = CRC;       // CRC ��.

    return Tx_Len;
}
//---------------------------------------------------------------------------




/*
Object Id   Object Name / Description   Type            category
0x00        VendorName                  ASCII String    Basic
0x01        ProductCode                 ASCII String
0x02        MajorMinorRevision          ASCII String

0x03        VendorUrl                   ASCII String    Regular
0x04        ProductName                 ASCII String 
0x05        ModelName                   ASCII String 
0x06        UserApplicationName         ASCII String
0x07-0x7F   Reserved

*/

int ModbusFunc_2B(unsigned char *buff, unsigned char NetAdr, unsigned char DeviceID, unsigned char ObjID)
{
    int Tx_Len = 0;
    buff[Tx_Len++] = NetAdr;//0x00;      // ����������������� - ����� ������������
    buff[Tx_Len++] = Func_0x2B; // �������
    buff[Tx_Len++] = 0x0E;      // MEI Type

    buff[Tx_Len++] = DeviceID;  // Device ID code
    buff[Tx_Len++] = ObjID;     // Object Id

    CRC = CRC16(&buff[0], Tx_Len);

    buff[Tx_Len++] = CRC>>8;       // CRC ��.
    buff[Tx_Len++] = CRC;          // CRC ��.

    return Tx_Len;
}

//---------------------------------------------------------------------------



int ModbusFunc_67(  unsigned char   *buff,
                    unsigned long   addr,
                    unsigned short  count,
                    unsigned char   Slave_Addr)
{
    int Tx_Len = 0;
    buff[Tx_Len++] = Slave_Addr;     // ����� ������������
    buff[Tx_Len++] = Func_0x67;      // �������

    buff[Tx_Len++] = addr >> 8;      // ����� �������� ��.
    buff[Tx_Len++] = addr;           // ����� �������� ��.

    buff[Tx_Len++] = count>>8;       // ������ ��.
    buff[Tx_Len++] = count;          // ������ ��.

    CRC = CRC16(&buff[0], Tx_Len);

    buff[Tx_Len++] = CRC>>8;    	 // CRC ��.
    buff[Tx_Len++] = CRC;            // CRC ��.

    return Tx_Len;
}
//---------------------------------------------------------------------------





int ModbusFunc_68(  unsigned char   *buff,
                    unsigned long   AddrInFile,
                    unsigned short  count,
                    unsigned char   Slave_Addr,
                    unsigned char   NumFile)
{
    int Tx_Len = 0;
    buff[Tx_Len++] = Slave_Addr;        // ����� ������������
    buff[Tx_Len++] = Func_0x68;         // �������
    buff[Tx_Len++] = NumFile;           // ����� �����

    buff[Tx_Len++] = AddrInFile >> 24;  // ����� �������� ��.
    buff[Tx_Len++] = AddrInFile >> 16;  // ����� ��������
    buff[Tx_Len++] = AddrInFile >> 8;   // ����� ��������
    buff[Tx_Len++] = AddrInFile >> 0;   // ����� �������� ��.

    buff[Tx_Len++] = count;             // ���-�� ����.

    CRC = CRC16(&buff[0], Tx_Len);

    buff[Tx_Len++] = CRC>>8;    	    // CRC ��.
    buff[Tx_Len++] = CRC;               // CRC ��.

    return Tx_Len;
}
//---------------------------------------------------------------------------




int ModbusFunc_69(  unsigned char   *buff,
                    unsigned long   AddrInFile,
                    unsigned short  *pData,
                    unsigned short  count,
                    unsigned char   Slave_Addr,
                    unsigned char   NumFile)
{
    int Tx_Len = 0;
    buff[Tx_Len++] = Slave_Addr;        // ����� ������������
    buff[Tx_Len++] = Func_0x69;         // �������
    buff[Tx_Len++] = NumFile;           // ����� �����

    buff[Tx_Len++] = AddrInFile >> 24;  // ����� �������� ��.
    buff[Tx_Len++] = AddrInFile >> 16;  // ����� ��������
    buff[Tx_Len++] = AddrInFile >> 8;   // ����� ��������
    buff[Tx_Len++] = AddrInFile >> 0;   // ����� �������� ��.

    buff[Tx_Len++] = count;             // ���-�� ����.

    while(count)
    {
        buff[Tx_Len++] = *pData>>8;
        buff[Tx_Len++] = *pData&0xFF;
        pData++;
        count--;
    }

    CRC = CRC16(&buff[0], Tx_Len);

    buff[Tx_Len++] = CRC>>8;    	    // CRC ��.
    buff[Tx_Len++] = CRC;               // CRC ��.

    return Tx_Len;
}
//---------------------------------------------------------------------------




//------- ���������� ���������� ���� � ������ �� ������ count ���� -------------
int GetLenAnswer(u08 func, u08 count)
{
    int Len = 0;

    switch (func)
    {
        case Func_0x03:
        case Func_0x04:
        case Func_0x67:
                    Len = count*2 + 5;
                    break;

        case Func_0x05:
        case Func_0x06:
        case Func_0x10:
                    Len = 8;//count*2 + 6;
                    break;

        case Func_0x2B:
                    Len = 0xFF;//count*2 + 5;
                    break;

        case Func_0x68:
                    Len = count*2 + 10;
                    break;

        case Func_0x69:
                    Len = 5;//count*2 + 10;
                    break;

        default:
                    break;
    }
    
    return Len;
}
#pragma package(smart_init)
