//---------------------------------------------------------------------------

#ifndef TVent_ModBusH
#define TVent_ModBusH

//#include <vcl.h>
//#include "types.h"
#include "TVent.h"


#define		MAX_SPEED	2200	// ���� �������� �����������. ������� �� ��� EbmPapst




#define		AllOk_merr	       		0
#define		Illegal_Function_merr	1
#define		Illegal_DataAdr_merr	2
#define		Illegal_DataVal_merr	3
#define		Failure_merr			4
#define 	SLAVE_DEVICE_BUSY_merr	6


#define NET_OFFSET		0
#define CMD_OFFSET		1
#define ADDR_OFFSET		2
#define QUAN_OFFSET		4
#define SIZE_OFFSET		2


typedef struct
{
	u16 	Start_ADDR;
	u16 	End_ADDR;
	u16 	Vnutr_ADDR;

//	u16 	*pRead;
}	Protokol_item;



class TVent_ModBus: public TVent
{
    private:
        void Init();

        u08 ErrMb(char *Bff, int code);
        u08 modbus_0x03(u08 *Bff, u08 len);
        u08 modbus_0x06(u08 *Bff, u08 len);
    public:

//    u16 *pTablProt;
//    static const u16 TablProt[13];
    u16 *pTablProtWR;


//    public:
    TVent_ModBus();
    TVent_ModBus(u16 _NetAdr);
    ~TVent_ModBus(){};

    u08 ProcessMsg(u08 *Bff, u08 len);
    void TimeProc();
    AnsiString Print(){return "ModBus NetAdr = " + AnsiString(this->NetAdr);};

};






//---------------------------------------------------------------------------
#endif
