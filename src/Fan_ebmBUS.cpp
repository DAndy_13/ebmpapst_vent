//---------------------------------------------------------------------------


#pragma hdrstop

#include "Fan_ebmBUS.h"

/*
const VentExpModule_type VentExpModuleMas[] = {
//    {Func_0x04,  (u16 *)&Vent[0].NetAdr,  (u16 *)&Vent[0].ActualSpeed,  2,     0xD010,    0, &Vent[0].Avar, 0},                     // ������ ������� ����������
//    {Func_0x04,  (u16 *)&Vent[0].NetAdr,  (u16 *)&Vent[0].Tmodule,      4,     0xD015,    0, &Vent[0].Avar, 0},                     // ������ ������� ����������
//    {Func_0x06,  (u16 *)&Vent[0].NetAdr,  0,                            1,     0xD001,    (u16 *)&Vent[0].Vtek, &Vent[0].Avar, &Vent[0].CnVtek},        // ��������� ��������
//    {Func_0x06,  (u16 *)&Vent[0].NetAdr,  0,                            1,     0xD102,    (u16 *)&Vent[0].Dir,  &Vent[0].Avar, &Vent[0].CnDir},         // ��������� ����������� ��������
};
*/



u8 CRC_ebmBUS(unsigned char *buf, unsigned char size)
{
    u8 CRC = 0xFF;

    for (; size > 0; size --){
        CRC ^= *buf++;
    }

    return CRC;//^0xFF;
}



u8 ebmGetStatus(unsigned char *buff, u8 addr, u8 GrAddr)
{
    u8 CRC = 0;
    u8 Tx_Len = 0;
    buff[Tx_Len++] = 0x35;                  // preamble
    buff[Tx_Len++] = CMD_GET_STATUS(addr);  // command byte
    buff[Tx_Len++] = GrAddr;                // group address
    buff[Tx_Len++] = 0;                     // data byte

    CRC = CRC_ebmBUS(&buff[0], Tx_Len);

    buff[Tx_Len++] = CRC;                   // CRC

    return Tx_Len;
}
//---------------------------------------------------------------------------


u8 ebmGetSpeed(unsigned char *buff, u8 addr, u8 GrAddr)
{
    u8 CRC = 0;
    u8 Tx_Len = 0;
    buff[Tx_Len++] = 0x15;                  // preamble
    buff[Tx_Len++] = CMD_GET_SPEED(addr);   // command byte
    buff[Tx_Len++] = GrAddr;                // group address
//    buff[Tx_Len++] = 0;                     // data byte

    CRC = CRC_ebmBUS(&buff[0], Tx_Len);

    buff[Tx_Len++] = CRC;                   // CRC

    return Tx_Len;
}
//---------------------------------------------------------------------------




u8 ebmSetSpeed(unsigned char *buff, u8 addr, u8 GrAddr, u8 data)
{
    u8 CRC = 0;
    u8 Tx_Len = 0;
    buff[Tx_Len++] = 0x35;                  // preamble
    buff[Tx_Len++] = CMD_SET_SPEED(addr);   // command byte
    buff[Tx_Len++] = GrAddr;                // group address
    buff[Tx_Len++] = data;                     // data byte

    CRC = CRC_ebmBUS(&buff[0], Tx_Len);

    buff[Tx_Len++] = CRC;                   // CRC

    return Tx_Len;
}
//---------------------------------------------------------------------------



u8 ebmSetRatio(unsigned char *buff, u8 addr, u8 GrAddr, u8 data)
{
    u8 CRC = 0;
    u8 Tx_Len = 0;
    buff[Tx_Len++] = 0x55;                  // preamble
    buff[Tx_Len++] = CMD_SET_EEPROM(addr);  // command byte
    buff[Tx_Len++] = GrAddr;                // group address
    buff[Tx_Len++] = 0x11;                  // EEPROM Adress
    buff[Tx_Len++] = data;                  // data byte 

    CRC = CRC_ebmBUS(&buff[0], Tx_Len);

    buff[Tx_Len++] = CRC;                   // CRC

    return Tx_Len;
}
//---------------------------------------------------------------------------

#pragma package(smart_init)
