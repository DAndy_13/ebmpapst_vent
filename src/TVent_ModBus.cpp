//---------------------------------------------------------------------------


#pragma hdrstop

#include "TVent_ModBus.h"
#include "crc_16.h"



Protokol_item TablProt[] =
// ���.���  ���.���  �����.���
{
    {0xD010, 0xD012, (u16)&OFFSET_Vent.ActualSpeed},
    {0xD015, 0xD019, (u16)&OFFSET_Vent.Tmodule},
//    {0xD018, 0xD019, (u16)&OFFSET_Vent.CurDir},
    {0xD102, 0xD103, (u16)&OFFSET_Vent.Dir},
    {0xFFFF,0xFFFF,0xFFFF}
};



Protokol_item TablProtWR[] =
// ���.���  ���.���  �����.���
{
    {0xD001, 0xD001, (u16)&OFFSET_Vent.Vtek},
    {0xD102, 0xD102, (u16)&OFFSET_Vent.Dir},
    {0xFFFF,0xFFFF,0xFFFF}
};


/*
    const u16 TVent_ModBus::TablProt[13] =
    // ���.���  ���.���  �����.���
    {
//        0xD010, 0xD011, TVent_ModBus::Data.zActualSpeed,
        0xD015, 0xD017, (u16)this->Temp.Tmodule,
        0xD018, 0xD018, (u16)this->Dir.CurDir,
        0xD102, 0xD103, (u16)this->Dir.Dir,
        0xFFFF };
*/
void TVent_ModBus::Init()
{
    this->MaxSpeed = MAX_SPEED;
    this->MaxValue = 64000;
}
//-------------------------------------------------

TVent_ModBus::TVent_ModBus(): TVent(0)
{
    this->Init();
}

TVent_ModBus::TVent_ModBus(u16 _NetAdr): TVent(_NetAdr)
{
    this->Init();
}
//-------------------------------------------------



void TVent_ModBus::TimeProc()
{
    float   Pers    = this->Data.VtekPers * 1.0;
    u16     Vtek    = this->Data.Vtek ;

   	this->Data.VtekPers         = (Vtek*100)/this->MaxValue;

    u16 ActualSpeed = this->Data.ActualSpeed;

    if(this->Data.CurDir != this->Data.Dir){            // ����� �����������
        if(ActualSpeed > 0){
            Vtek = 0;
        }
        else{
            this->Data.CurDir = this->Data.Dir;
        }
    }

    if(ActualSpeed < Vtek){                             // ������
        ActualSpeed = ActualSpeed + (Vtek - ActualSpeed)*0.1 +1;
    }
    else if(ActualSpeed > Vtek){                        // ����������
        ActualSpeed = ActualSpeed - (ActualSpeed - Vtek)*0.05;
    }

   	this->Data.ActualSpeed  = ActualSpeed;
  	this->Data.SpeedObMin   = (ActualSpeed*this->MaxSpeed)/this->MaxValue;
}
//-------------------------------------------------




u08 TVent_ModBus::ProcessMsg(u08 *Bff, u08 len)
{
    int sz, CrcVal;
  
    if ((Bff[NET_OFFSET] != this->NetAdr) || CRC16(Bff,len)) return 0;//�������� ������ � ��
    switch (Bff[CMD_OFFSET])     // ����� �������
    {
        case 0x03:
        case 0x04: sz = modbus_0x03(Bff,len); break;//������� 03 � 04
        case 0x06: sz = modbus_0x06(Bff,len); break;
//      case 0x10: sz = modbus_0x10(Bff,len); break;
        default:
            Bff[CMD_OFFSET] |= 0x80;
            Bff[SIZE_OFFSET] = 1;
            sz = 3;
    }
  
    if ((!sz) || (sz > 255)) return 0;
    CrcVal = CRC16(Bff, sz);
    Bff += sz;
    *Bff++ = (CrcVal>>8);
    *Bff++ = CrcVal&0xFF;

    return sz + 2;
}
//-------------------------------------------------


//****************************************************************************
// ������������ ������ �� ������
//  =>* Bff  - ��������� �� ������� ������
//      code - ��� ������
//  *=> ����� ������
//****************************************************************************
u08 TVent_ModBus::ErrMb(char *Bff, int code)
{
    Bff[CMD_OFFSET] |= 0x80;
    Bff[SIZE_OFFSET] = code;
    return 3;
}




u08 TVent_ModBus::modbus_0x03(u08 *Bff, u08 len)
{	
	u16 Adr, tmpAdr;
	u16 size;

	int outpars_;
	char *pnt;
	u16 *pt1;
  	u08 index = 0;

	Adr     = (Bff[ADDR_OFFSET] << 8) | Bff[ADDR_OFFSET+1]; // ������� ��������� ����� ��������
	size    = (Bff[QUAN_OFFSET] << 8) | Bff[QUAN_OFFSET+1];// ������� ���������� ���������

	outpars_ = 0;

	pt1 = &TablProt[index].Start_ADDR;
	while (*pt1 != 0xFFFF)
	{ 
		if ((Adr >= *pt1) && (Adr <= *(pt1+1)))
		{ 
		  outpars_ = *(pt1+1) - Adr;	
		  Adr = *(pt1+2) + (Adr - *pt1);
		  break; 
		}
		pt1 = &TablProt[++index].Start_ADDR;
	}

    if ((size>125) || (size<1) || (size>outpars_))
		return ErrMb(Bff, Illegal_DataVal_merr);
        

	pnt = (char*)(Bff + SIZE_OFFSET);    	// ������ �� NetAdr, Func
	*pnt++ = size * 2;          			// ByteCount

	// ������ �� ���
	{
		char *pRead;// = (char *)pt1;
        pRead = (char *)&this->Data;
//		pRead = (char *)(TablProt[index].pRead);
		pRead += Adr;//*2;

		for (; size>0; size--, Adr++)
		{ 
			*pnt++ = *(pRead + 1);
			*pnt++ = *pRead;
			pRead += 2;
		}
	}	
	
	return Bff[SIZE_OFFSET]+3;
}


//****************************************************************************
// ����� � ������ ��������
//  =>* Bff    - ��������� �� ������� ������
//      len    - ����� �������� �������
//  *=> ����� ������
//****************************************************************************
u08 TVent_ModBus::modbus_0x06(u08 *Bff, u08 len)
{
	u16 Adr;//, tmpAdr;
	u16 Data;

	u16 *pt1; 
  	u08 index = 0;

	Adr     = (Bff[ADDR_OFFSET] << 8) | Bff[ADDR_OFFSET+1];
	Data    = (Bff[QUAN_OFFSET] << 8) | Bff[QUAN_OFFSET+1];

	pt1 = &TablProtWR[index].Start_ADDR;

	while (*pt1 != 0xFFFF)
	{ 
		if ((Adr >= *pt1) && (Adr <= *(pt1+1)))
		{ 
		  Adr = *(pt1+2) + (Adr - *pt1);
		  break; 
		}
		pt1 = &TablProtWR[++index].Start_ADDR;
	}

	if (*pt1 == 0xFFFF)
		return ErrMb(Bff, Illegal_DataAdr_merr);	  


    // ������ � ���
	{	
		//pt1 = ((unsigned short*)&Triol.RIZOL + Adr);
        pt1 = (u16 *)&this->Data;
        pt1 += Adr/2;

		*pt1 = Data;		
	}	
	//else return ErrMb(ModbusSlave.Buf, Failure_merr);

	return 7;	
}
//---------------------------------------------------------------------------

#pragma package(smart_init)
