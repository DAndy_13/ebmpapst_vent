//---------------------------------------------------------------------------

#include <vcl.h>
#include <stdio.h>
#pragma hdrstop

#include "modbus.h"
#include "Fan_ebmBUS.h" 
#include "TAnimation.h"

#include "types.h"
#include "main.h"
//#include "TVent.h"
#include "TVent_ModBus.h"
#include "TVent_EbmBus.h"

#include "ComPort_Unit.h"
#include "TComManager.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "sButton"
#pragma link "sSkinManager"
#pragma link "sComboBox"
#pragma link "sPanel"
#pragma link "sTrackBar"
#pragma link "sGroupBox"
#pragma link "sCheckBox"
#pragma resource "*.dfm"
TF_Main *F_Main;

TComManager *pComManager = 0;


mVent_type mVent = {0};



#define CELL_SIZE 50  
#define MAX_COUNT 16
TVent *pVent = 0;
TVent *VentArr[MAX_COUNT+1] = {0};  // ������ ���������� (���!)


Graphics::TBitmap *Buffer; //��������� �����.
TAnime *ani;               //����� ��������.




/********************************************************************
********** ������� ���������
*********************************************************************/
void __fastcall TF_Main::WmUser_catch (TMessage &msg)
{
    //static int i = 0;

    if ( msg.Msg == WM_REFRESH ){ // �������� �������
        StringGrid1->Refresh();
        Application->ProcessMessages();
    }
}
//---------------------------------------------------------------------------




__fastcall TF_Main::TF_Main(TComponent* Owner)
    : TForm(Owner)
{
    mVent.VentCount = 10;
    mVent.VentType  = VentModBus;
    bNullCell = true;
}
//---------------------------------------------------------------------------




TVent *pVent_ModBus = new TVent_ModBus(1);

void __fastcall TF_Main::Button1Click(TObject *Sender)
{

    pVent_ModBus = VentArr[0];

    u08 Buff[100] = {0};
    u08 sz = 0;
    static u16 Speed = 0;
    for(int i = 0; i < mVent.VentCount; i++){
        // ��������� �����������
        sz = ModbusFunc_06(Buff, 0xD001, Speed, i+1);
        pVent_ModBus = VentArr[i];
        pVent_ModBus->ProcessMsg(Buff, sz);
    }
    Speed +=6400;

}
//---------------------------------------------------------------------------



void TF_Main::GridIni(TStringGrid *Grid, u08 _VentCount, enVentType _VentType)
{
    int i = 0;//, j = 0;

    for(int mRow = 1; mRow < Grid->RowCount; mRow++){
        Grid->Rows[mRow]->Clear();
    }

    Grid->RowCount = 2;// + _VentCount;
    Grid->FixedRows = 1;
    if(mVent.VentType == VentModBus){
        Grid->ColCount = 9;
    }
    else{
       Grid->ColCount = 5;
    }

    Grid->FixedCols = 1;

    Grid->ColWidths[i] = 50;
    Grid->Cells[i++][0] = "NetAdr";

    Grid->ColWidths[i] = 50;
    Grid->Cells[i++][0] = "Fan";

    Grid->ColWidths[i] = 50;
    Grid->Cells[i++][0] = "Vtek";

    Grid->ColWidths[i] = 100;
    Grid->Cells[i++][0] = "ActualSpeed";

    Grid->ColWidths[i] = 70;
    Grid->Cells[i++][0] = "State";

    if(mVent.VentType == VentModBus){
    Grid->ColWidths[i] = 80;
    Grid->Cells[i++][0] = "Tmodule";

    Grid->ColWidths[i] = 80;
    Grid->Cells[i++][0] = "Tmotor";

    Grid->ColWidths[i] = 80;
    Grid->Cells[i++][0] = "Tint";

    Grid->ColWidths[i] = 150;
    Grid->Cells[i++][0] = "Dir";
    }
}
//---------------------------------------------------------------------------




void TF_Main::GridFill(TStringGrid *Grid, TVent *pVent)
{
//    char ch[20] = {0};
    if(bNullCell)  bNullCell = false;
    else
    Grid->RowCount++;
    int indx = Grid->RowCount - Grid->FixedRows;


    int i = 0;
    Grid->Objects[i][indx] = (TObject *)pVent;

}
//---------------------------------------------------------------------------




void __fastcall TF_Main::TypeComboChange(TObject *Sender)
{
    mVent.VentType = (enVentType)((TsComboBox *)Sender)->ItemIndex;
}
//---------------------------------------------------------------------------



void __fastcall TF_Main::CountComboChange(TObject *Sender)
{
    mVent.VentCount = CountCombo->ItemIndex+1;
}
//---------------------------------------------------------------------------


void __fastcall TF_Main::sButton2Click(TObject *Sender)
{
    CountComboChange(CountCombo);

    AnsiString _temp_name = ExtractFileDir(Application->ExeName) + "\\img_fan50\\";
    char ch[16] = {0};
    TAnime *pAni = 0;
    TVent *pVent = 0;

    TsButton *pButt = (TsButton *)Sender;
    TStringGrid *Grid = StringGrid1;
    
    AnsiString  str;
    AnsiString  Port        = PortCombo->Text;
    int         BaudRate;
    if(mVent.VentType == VentModBus){
        BaudRate = 19200;
    }
    else{
        BaudRate = 9600;
    }

    if(Port == ""){
        ShowMessage("�� ������ ����!!!");
        return;
    }

    
    if(pButt->Caption == "Start"){

        str = COMOpen(&hCOMport, Port, BaudRate);
        if(str == "")
        {
            pButt->Caption = "Stop";
            CountCombo->Enabled     = false;
            TypeCombo->Enabled      = false;
            PortCombo->Enabled      = false;
            MainTimer->Enabled      = true;
            AnimationTimer->Enabled = true;

            for(int ARow = 0; ARow < mVent.VentCount; ARow++){
                pAni = (TAnime *)Grid->Objects[1][ARow];
                if(pAni){
                    delete pAni;
                    pAni = 0;
                }
                pVent = (TVent *)Grid->Objects[0][ARow];
                if(pVent){
                    delete pVent;
                    pVent = 0;
                    VentArr[ARow] = 0;
                }

            }

            GridIni(Grid, mVent.VentCount, mVent.VentType);

            Grid->ColWidths[1]   = CELL_SIZE;
            for(int i = 0; i < mVent.VentCount; i++){
                if(mVent.VentType == VentModBus){
                    VentArr[i] = new TVent_ModBus(i+1);
                    GridFill(Grid, VentArr[i]);
                }
                else{
                    VentArr[i] = new TVent_EbmBus(i+1);
                    GridFill(Grid, VentArr[i]);
                }
            }
            bNullCell = true;


            for(int i = 0; i < mVent.VentCount; i++)
            {
                Grid->RowHeights[i+1]  = CELL_SIZE;
                pAni = new TAnime();
                Grid->Objects[1][i+1] = (TObject *)pAni;

                pAni->Clear();
                //for(int i=0; i<=24/2; i+=2){
                for(int i=0; i<=24; i++){
                    sprintf(ch, "fan_%02d.bmp", i);
                    pAni->AddPage(_temp_name + AnsiString(ch));
                }
            }

            
            pComManager = new TComManager(false);     // true = ����������
        }
        else
        {
            ShowMessage(str);
            return;
        }
    }
    else{ // "Stop"
        if(pComManager)
        {
            pComManager->Terminate();
            pComManager->WaitFor();
            pComManager = 0;
        }
        COMClose(&hCOMport);

         
        pButt->Caption = "Start";
        CountCombo->Enabled     = true;
        TypeCombo->Enabled      = true;
        PortCombo->Enabled      = true;
        MainTimer->Enabled      = false;
        AnimationTimer->Enabled = false;

    }
}
//---------------------------------------------------------------------------




void TF_Main::SetBkColor(TStringGrid *Grid, TColor Color)
{
    Grid->Canvas->Brush->Color = Color;
    Grid->Canvas->Font->Color = clBlack;
//    Grid->Canvas->FillRect(Rect);
}





void __fastcall TF_Main::StringGrid1DrawCell(TObject *Sender, int ACol,
      int ARow, TRect &Rect, TGridDrawState State)
{
    TStringGrid *Grid = (TStringGrid *)Sender;
    TVent *pVent;// = (myDescriptorPrm_type*)Grid->Objects[ACol][ARow];


    int iCol = 0;
    for (iCol = 0; iCol < Grid->ColCount; iCol++)
        if(Grid->Cells[iCol][0] == "NetAdr")
            break;

    pVent = (TVent*)Grid->Objects[iCol][ARow];

    TColor mColor = clLime;//clGreen;//Grid->Canvas->Brush->Color;


    if(pVent)
    {
        if((Grid->Cells[ACol][0] == "NetAdr"))
        {
            Grid->Canvas->FillRect(Rect);
            Grid->Canvas->TextOut(Rect.Left+6, Rect.Top, pVent->NetAdr);
        }
        else if((Grid->Cells[ACol][0] == "Vtek"))
        {
            //SetBkColor(Grid, (TColor)clRed);
            Grid->Canvas->FillRect(Rect);
            Grid->Canvas->TextOut(Rect.Left+6, Rect.Top, pVent->Data.VtekPers);
        }
        else if((Grid->Cells[ACol][0] == "ActualSpeed"))
        {
            Grid->Canvas->FillRect(Rect);
            Grid->Canvas->TextOut(Rect.Left+6, Rect.Top, pVent->Data.SpeedObMin);//pVent->Data.ActualSpeed);
        }
        else if(Grid->Cells[ACol][0] == "State")
        {
            Grid->Canvas->FillRect(Rect);
            Grid->Canvas->TextOut(Rect.Left+6, Rect.Top, pVent->Data.State);
        }
        else if(Grid->Cells[ACol][0] == "Tmodule")
        {
            SetBkColor(Grid, (TColor)mColor);
            Grid->Canvas->FillRect(Rect);
            Grid->Canvas->TextOut(Rect.Left+6, Rect.Top, pVent->Data.Tmodule);
        }
        else if(Grid->Cells[ACol][0] == "Tmotor")
        {
            SetBkColor(Grid, (TColor)mColor);
            Grid->Canvas->FillRect(Rect);
            Grid->Canvas->TextOut(Rect.Left+6, Rect.Top, pVent->Data.Tmotor);
        }
        else if(Grid->Cells[ACol][0] == "Tint")
        {
            SetBkColor(Grid, (TColor)mColor);
            Grid->Canvas->FillRect(Rect);
            Grid->Canvas->TextOut(Rect.Left+6, Rect.Top, pVent->Data.Tint);
        }
        else if(Grid->Cells[ACol][0] == "Dir")
        {
            Grid->Canvas->FillRect(Rect);
            //Grid->Canvas->TextOut(Rect.Left+6, Rect.Top, pVent->Data.Dir);

            if(pVent->Data.Dir)
                Grid->Canvas->TextOut(Rect.Left+6, Rect.Top, "������");//"�� �������");
            else
                Grid->Canvas->TextOut(Rect.Left+6, Rect.Top, "��������");//"������ �������");
            
        }
    }

    if(ACol != 1)
        return;
        
    TAnime *pAni = (TAnime *)Grid->Objects[ACol][ARow];
    if(!pAni)
        return;

    pAni->Draw(Buffer->Canvas,0,0);
    Grid->Canvas->CopyRect(Rect, Buffer->Canvas, Buffer->Canvas->ClipRect);


}
//---------------------------------------------------------------------------


TsTrackBar *pTrBar;// = sTrackBar1;

void TF_Main::GridShowEdit(TStringGrid *Grid, int ACol, int ARow, u16 Value)//TVent *pVent)
{
    TRect Rect;
    Rect = Grid->CellRect(ACol, ARow);
    Rect.Left      = Rect.Left + Grid->Left;
    Rect.Right     = Rect.Right + Grid->Left;
    Rect.Top       = Rect.Top + Grid->Top;
    Rect.Bottom    = Rect.Bottom + Grid->Top;

//    if(mPrm->FlgPrm.TypeVar == NtxtFrmt)
    {
        pTrBar->Position = Value;

        pTrBar->Left     = Rect.Left + 1;
        pTrBar->Top      = Rect.Top + 1;
        pTrBar->Width    = ((Rect.Right + 1) - Rect.Left);
        pTrBar->Height   = Rect.Height()+1;

        pTrBar->Show();
        pTrBar->BringToFront();
        pTrBar->SetFocus();
    }
}





void __fastcall TF_Main::StringGrid1DblClick(TObject *Sender)
{
    TStringGrid *Grid = StringGrid1;
    int ACol = Grid->Col;
    int ARow = Grid->Row;

    TVent *pVent = (TVent*)Grid->Objects[0][ARow];
    if(!pVent) return;

    if(ARow >0){
        if(Grid->Cells[ACol][0] == "Tmodule"){
            pTrBar = sTrackBar1;
            GridShowEdit(Grid, ACol, ARow, pVent->Data.Tmodule);
        }
        else if(Grid->Cells[ACol][0] == "Tmotor"){
            pTrBar = sTrackBar1;
            GridShowEdit(Grid, ACol, ARow, pVent->Data.Tmotor);
        }
        else if(Grid->Cells[ACol][0] == "Tint"){
            pTrBar = sTrackBar1;
            GridShowEdit(Grid, ACol, ARow, pVent->Data.Tint);
        }
        else if(Grid->Cells[ACol][0] == "Vtek"){
            //pTrBar = sTrackBar2;
            //GridShowEdit(Grid, ACol, ARow, pVent->Data.VtekPers);
        }

    }
}
//---------------------------------------------------------------------------

void __fastcall TF_Main::sTrackBar1Exit(TObject *Sender)
{
    //TsMaskEdit *pMask = (TsMaskEdit *)Sender;
    TsTrackBar *pTrBar = (TsTrackBar *)Sender;

    TStringGrid *Grid = StringGrid1;

    int ACol = Grid->Col;
    int ARow = Grid->Row;

    TVent *pVent = (TVent*)Grid->Objects[0][ARow];
    if(pVent)
    {
        if(Grid->Cells[ACol][0] == "Tmodule"){
            pVent->Data.Tmodule = (u16)pTrBar->Position;
        }
        else if(Grid->Cells[ACol][0] == "Tmotor"){
            pVent->Data.Tmotor = (u16)pTrBar->Position;
        }
        else if(Grid->Cells[ACol][0] == "Tint"){
            pVent->Data.Tint = (u16)pTrBar->Position;
        }
        else if(Grid->Cells[ACol][0] == "Vtek"){
            pVent->Data.VtekPers = (u16)pTrBar->Position;
        }
        //ShowMessage("OnExit");
        pTrBar->Hide();
//        Grid->Refresh();
        PostMessage(F_Main->Handle, WM_REFRESH, 0, 0);
    }
}
//---------------------------------------------------------------------------



void __fastcall TF_Main::FormCreate(TObject *Sender)
{
    F_Main->DoubleBuffered = true;
    StringGrid1->DoubleBuffered = true;

    CountCombo->Items->Clear();

    for(int Indx = 0; Indx < MAX_COUNT; Indx++){
        CountCombo->Items->Add(Indx+1);
    }

    CountCombo->ItemIndex = 4;
    mVent.VentCount = CountCombo->ItemIndex;


    EnumerateComPorts((TComboBox*)PortCombo);
    PortCombo->ItemIndex = -1;



    //�������� ��������
    ani = new TAnime();

    //�������� �����.
    Buffer = new Graphics::TBitmap();
    //������ ������ ������ ��������� � �������� PaintBox
    Buffer->Height = CELL_SIZE;//100;//Image1->Height;
    Buffer->Width  = CELL_SIZE;//100;//Image1->Width;
    Buffer->Canvas->Rectangle(0,0,CELL_SIZE,CELL_SIZE);

}
//---------------------------------------------------------------------------


void __fastcall TF_Main::Button2Click(TObject *Sender)
{
    TStringGrid *Grid = StringGrid1;
    Grid->RowHeights[3] = 100;
    Grid->ColWidths[1]  = 100;
}
//---------------------------------------------------------------------------


void __fastcall TF_Main::sCheckBox1Click(TObject *Sender)
{
    TsCheckBox *pChBox = (TsCheckBox *)Sender;

    if(pChBox->Checked)
        pChBox->Caption = "�� �������";
    else
        pChBox->Caption = "������ �������";

    u08 Buff[100] = {0};
    u08 sz = 0;

    bool Dir = sCheckBox1->Checked;
    for(int i = 0; i < mVent.VentCount; i++){
        // ��������� �����������
        sz = ModbusFunc_06(Buff, 0xD102, (u16)Dir, i+1);
        pVent_ModBus = VentArr[i];
        pVent_ModBus->ProcessMsg(Buff, sz);
    }        
}
//---------------------------------------------------------------------------


void __fastcall TF_Main::Button3Click(TObject *Sender)
{
    TVent *pVent;
    pVent = VentArr[0];

    u08 NetAdr = 1;

    u08 Buff[100] = {0};
    u08 sz = 0;
    /*
    sz = ebmGetStatus(Buff, NetAdr, 1);
    pVent->ProcessMsg(Buff, sz);

    sz = ebmGetSpeed(Buff, NetAdr, 1);
    pVent->ProcessMsg(Buff, sz);
    */
    static u08 Speed = 0;
    for(int i = 0; i < mVent.VentCount; i++){
        NetAdr = i+1;
        sz = ebmSetSpeed(Buff, NetAdr, i+1, Speed);
        pVent = VentArr[i];
        pVent->ProcessMsg(Buff, sz);
    }
    Speed += 25;
}
//---------------------------------------------------------------------------


void __fastcall TF_Main::MainTimerTimer(TObject *Sender)
{
    TVent *pVent;
    for(int i = 0; i < mVent.VentCount; i++){
        pVent = VentArr[i];
        if(pVent){
            pVent->TimeProc();
            //PostMessage(F_Main->Handle, WM_REFRESH, 0, 0);
//            StringGrid1->Refresh();
//            StringGrid1->Repaint();
//            Application->ProcessMessages();
        }
    }
}
//---------------------------------------------------------------------------


                 


void __fastcall TF_Main::AnimationTimerTimer(TObject *Sender)
{
    TStringGrid *Grid = StringGrid1;
    TVent *pVent;
    u16 Pers = 0;
    u16 Interval = 0;

    for(int ARow = 0; ARow < Grid->RowCount; ARow++){
//        Buffer->Canvas->Rectangle(0,0,90,90);
        TAnime *pAni = (TAnime *)Grid->Objects[1][ARow];
        pVent = (TVent*)Grid->Objects[0][ARow];
        if(!pAni || !pVent)
            continue;

        Pers = (pVent->Data.SpeedObMin*100)/pVent->MaxSpeed;
        Interval = 50 - Pers/2;
        //Interval = 100 - Pers;
        pAni->SetInterval(Interval);
        pAni->InReverse(pVent->Data.CurDir);

        pAni->Draw(Buffer->Canvas,0,0);
        if(Interval < 50){
            pAni->Change();
        }
        //PostMessage(F_Main->Handle, WM_REFRESH, 0, 0);

        //TRect Rect = Grid->CellRect(1, ARow);
        //Grid->Canvas->CopyRect(Rect, Buffer->Canvas, Buffer->Canvas->ClipRect);
    }
    
    StringGrid1->Refresh();

}
//---------------------------------------------------------------------------

