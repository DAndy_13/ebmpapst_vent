//---------------------------------------------------------------------------


#pragma hdrstop

#include "TAnimation.h"



TAnime::TAnime()
{
    _interval = 1;
    _tmp_int = _interval;
    _reverse = true;
    _NumFrame = 0;
    _tm = 0;
}
//---------------------------------------------------------------------------



TAnime::~TAnime()
{
    /*******************************************************
        ������� ������ �����������, ��� ��� ������ �������
        ����� ����������� ������ ��������� �� �����
        ����������� TBitmap.
    */
    Clear();
}
//---------------------------------------------------------------------------



void TAnime::Clear()
{
    Graphics::TBitmap *_temp;

    /*******************************************************
        �������� ��������� � ���� TBitmap � ��������
        �������� delete.
    */
    for(int i = 0; i < (int) _frame_points.size(); i ++ ){
        _temp = (Graphics::TBitmap *) _frame_points[i];
        delete _temp;
    }
}
//---------------------------------------------------------------------------



void TAnime::AddPage(AnsiString FileName)
{
    /*******************************************************
        �������� ������ TBitmap � �������� � ���� ����.
        � ������ ��������� ������� �������� � ���������.
    */

    Graphics::TBitmap *_temp = new Graphics::TBitmap();
    _temp->LoadFromFile(FileName);
    _frame_points.push_back((int) _temp);

    //������� ������ ������.
}
//---------------------------------------------------------------------------


void TAnime::SetInterval(int __interval)
{
    _interval = __interval;
}
//---------------------------------------------------------------------------

/*
void TAnime::InReverse()
{
    _reverse ? _reverse = false : _reverse = true;
}
*/
//---------------------------------------------------------------------------



void TAnime::Draw(TCanvas *c, int X, int Y)
{
    /*******************************************************
        ��������� ����.
        �������� ��� (Graphics::TBitmap *), �����
        ���, ��� ����������� �� ������� ���������.
    */

    c->Draw(X,Y, (Graphics::TBitmap *) _frame_points[_NumFrame] );

}


void TAnime::Change()
{
    /*******************************************************
        ������ �������� �������� ��������
    */
    if(_tm >= (int) _tmp_int){//_interval){
        _tm = 0;
        _tmp_int = _interval;

        if(!_reverse){
            _NumFrame --;
            if(_NumFrame < 0)
                _NumFrame = (int) _frame_points.size() - 1;
        }
        else{
            _NumFrame ++;
            if(_NumFrame >= (int) _frame_points.size())
                _NumFrame = 0;
        }
    }
    else _tm++;
}
//===========================================================================




//---------------------------------------------------------------------------

#pragma package(smart_init)
